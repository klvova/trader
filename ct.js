const Web3 = require('web3');
const net = require('net');
const web3 = new Web3('/root/jsonrpc.ipc', net);
const abiArray = require('./abi.js');
const contractAddress = '0x8d12A197cB00D4747a1fe03395095ce2A5CC6819';
const contract = new web3.eth.Contract(abiArray, contractAddress);

contract.events.Order({fromBlock: 4580408})
.on('data', console.log)
.on('changed', console.log)
.on('error', console.error);
