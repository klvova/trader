//@flow

const models = require('./models');
const { TempEvents } = models;

const createRow = (type: TempEventType, event: Object) => TempEvents.create({
    type: type,
    event: event
});


export {
    createRow
}