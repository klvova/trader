const io = require('socket.io-client');
const Web3 = require('web3');
const BigNumber = require('bignumber.js');
// const BigNumber = Web3.utils.BN;
const sha256 = require('./utils/formatData').sha256;
const tokenAddr = {addr: '0x419c4db4b9e25d6db2ad9691ccb832c8d9fda05e', decimals: 18};
let possibleInteresting = [];
const lodash = require('lodash');
const fs = require('fs');
const Telegraf = require('telegraf');
var HttpsProxyAgent = require('https-proxy-agent');
var url = require('url');

var http = require('http');


const chatIds = [124442878];


// const BOT_KEY = '413996458:AAFKgXYCj-UpuQPmnNL4e9MwJPXuXBOecMs';
//
// const bot = new Telegraf(BOT_KEY)
// bot.start((ctx) => {
//     console.log('started:', ctx.from.id)
//     return ctx.reply('Welcome!')
// })
// bot.startPolling()


const orderMap = {

};


const servers = [
    'https://socket01.etherdelta.com',
    'https://socket02.etherdelta.com',
    'https://socket03.etherdelta.com',
    'https://socket04.etherdelta.com',
    'https://socket05.etherdelta.com',
    'https://socket06.etherdelta.com'
];

const sleep = (time) => new Promise((r) => setTimeout(r, time));

const socketPool = {};
const getClientByToken = (token) => {
    const socketUrl = servers[Math.floor(Math.random()*servers.length)];
    if (!socketPool[token])
    {
        var username = 'lum-customer-hl_587681f9-zone-zone1';
        var password = 'm1arf8cgewt2';
        var session_id = (1000000 * Math.random())|0;
        var port = 22225;
        var super_proxy = 'http://'+username+'-session-'+session_id+':'+password+'@zproxy.luminati.io:'+port;
        var agent = new HttpsProxyAgent(url.parse(super_proxy));
        // agent.proxy = super_proxy;
        const socket = io.connect(socketUrl, { transports: ['websocket'], agent });
        socket.on('connect', () => {
            console.log('socket connected', token, socketUrl);
        });
        socket.on('disconnect', () => {
            console.log('socket disconnected', token, socketUrl);
        });
        socketPool[token] = socket;
    }
    return socketPool[token];
};

function Service() {
    const self = this;

    self.init = config => new Promise((resolve, reject) => {
        self.config = config;
        self.state = {
            orders: {},
            trades: undefined,
            myOrders: undefined,
            myTrades: undefined,
            listeners: {}
        };
        self.lastUpdate = {};

        // self.socket = io.connect(self.config.socketURL, { transports: ['websocket'] });
        // self.socket.on('connect', () => {
        //     console.log('socket connected');
        //     resolve();
        // });
        //
        // self.socket.on('disconnect', () => {
        //     console.log('socket disconnected');
        // });
        //
        // setTimeout(() => {
        //     reject('Could not connect to socket');
        // }, 10000);
    });

    self.getMarket = (token) => {
        if (!token || !Web3.utils.isAddress(token.addr)) throw new Error('Please enter a valid token');
        const socket = getClientByToken(token.addr);
        socket.emit('getMarket', { token: token.addr });
        console.log('emit get market');
    };

    self.waitForMarket = (token, isActive) => new Promise((resolve, reject) => {
        if (self.state.listeners[token.addr])
            return;
        self.state.listeners[token.addr] = true;
        self.lastUpdate[token.addr] = 0;
        const socket = getClientByToken(token.addr);
        setTimeout(() => {
            reject('Could not get market');
        }, 20000);
        // self.state = {
        //     orders: undefined,
        //     trades: undefined,
        //     myOrders: undefined,
        //     myTrades: undefined,
        // };
        socket.off('orders');
        // self.socket.off('trades');
        const getMarketAndWait = () => {
            self.getMarket(token);
            socket.once('market', async (market) => {

                if ((market.orders)) {
                    console.log('tickers', market.returnTicker);
                    if (market.returnTicker)
                    {
                        possibleInteresting = [];
                        Object.keys(market.returnTicker).forEach((k) => {
                            const ticker = market.returnTicker[k];
                            if (ticker.ask < ticker.bid)
                                possibleInteresting.push({...ticker, ticker: k});
                        });
                        console.log(possibleInteresting);
                        for (let x of possibleInteresting)
                        {
                            self.waitForMarket({addr: x.tokenAddr}).catch(console.error);
                            await sleep(2000);
                        }
                    }
                    else
                    {
                        setTimeout(() => {
                            if (possibleInteresting.length == 0)
                            {
                                console.log('cannot get market tickers');
                                process.exit(1);
                            }
                            getMarketAndWait();
                        }, 12000);
                    }
                    resolve();
                } else {
                    console.log('market answer', market);
                    setTimeout(() => {
                        if (possibleInteresting.length == 0)
                        {
                            console.log('cannot get market tickers');
                            process.exit(1);
                        }
                        getMarketAndWait();
                    }, 5000);
                }
            });
        };
        getMarketAndWait();
    });
}

module.exports = Service;


var s= new Service();
s.init({socketURL: 'https://socket05.etherdelta.com'});
s.waitForMarket(tokenAddr, true).catch(console.error);