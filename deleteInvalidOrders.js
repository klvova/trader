const { deleteOrders } = require('./modelsLayer');
const { deletedTime } = require('./constants');

function launchDeleteInvalidOrders(){
    deleteOrders();
    setInterval(deleteOrders, deletedTime);
}

module.exports = {
    launchDeleteInvalidOrders
};
