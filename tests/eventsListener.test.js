import 'jest';
import { createOrder } from '../modelsLayer';
import EventsListener from '../eventsListener';
import { orderEvent as orderEventData} from './data';
import MockContract from './mockContract';
// import { orderCb } from '../eventsListener';


describe('eventsListener', () => {
    let contract;
    let listener;

    beforeEach(done => {
        contract = new MockContract();
        listener = new EventsListener(contract, null);
        done();
    });

    test('createOrder', () => {
        let orderCb =  jest.fn();

        listener.order(orderCb);

        contract.trigger('data',orderEventData );

        expect(orderCb).toHaveBeenCalledWith(orderEventData);

    })
});