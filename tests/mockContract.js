let arr = {};
class MockContract{
    constructor() {
        const self = this;
        this.arr = {};
        this.events = {
            Order() {
                return {
                    on(type, cb) {
                        if (!arr) {
                            self.arr= {};
                        }
                        if (!arr[type]) {
                            self.arr[type] = [];
                        }
                        self.arr[type].push(cb);
                        return this;
                    }
                }
            }
        }
    }

    trigger(type, data) {
        this.arr[type].forEach((cb) => cb(data));
    }
};

export default MockContract;