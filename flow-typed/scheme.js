interface Contract{
    events: {[event: string]: ContractEventGenerator},
    methods: any,
}

type ContractEventGenerator = (options?: {filter?: Object, fromBlock?: number, topics?: [any]}, callback?: Function) => ContractEventEmitter

interface ContractEventEmitter{
    on: (eventName: string, callback: (event: Object) => any) => any
}

type BNOP = (BN) => BN;

interface BN{
    add: BNOP,
    sub: BNOP,
    mul: BNOP,
    lt: BNOP,
    lte: BNOP,
    gt: BNOP,
    gte: BNOP
}

interface ContractEvent{
    returnValues: Object,
    raw: {
        data: string,
        topics: Array<string>
    },
    event: string,
    signature: string,
    logIndex: number,
    transactionIndex: number,
    transactionHash: string,
    blockHash: string,
    blockNumber: number,
    address: string,
    removed: boolean
}

interface OrderDBSirnature {
    tokenGet: string,
    amountGet: string,
    tokenGive: string,
    amountGive: string,
    expires: string,
    nonce: string,
    deleted: boolean,
    user: string,
    hash: string,
    timestamp: number,
    actualAmountGive: string,
    actualAmountGet: string,
    transaction_hash: string
}


type TempEventType =  "create_order" | "cancel_order" | "create_trade" | "cancel_trade";

interface Web3Interface{
    eth: {
        Contract: Contract,
        getBalance: (string) => Promise<string>,
        getGasPrice: () => Promise<string>
    },
    utils: {
        toBN: (string) => BN
    }
}

type TransactionType = 'trade';


type TokenBalance = {
    [tokenAddress: string]: string
};