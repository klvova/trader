// @flow

const Web3 = require('web3');
const account = require('eth-lib/lib/account');

const muchLimit = '1000000000000';

const priceMul = '2';
const priceDiff = '1000';
const ethereumUnit = '1000000000000000000';
const takeFee = '5000000000000000';
const makeFee = '5000000000000000';

class Trader{
    contract: Contract;
    privateKey: string;
    address: string;
    web3: Web3Interface;
    constructor(contract: Contract, privateKey: string, web3: Web3Interface){
        this.contract = contract;
        this.privateKey = privateKey;
        this.web3 = web3;
        this.address = account.fromPrivate(privateKey);
    }
    async getEthereumBalance(): Promise<BN>{
        const balance = await this.web3.eth.getBalance(this.address);
        return this.web3.utils.toBN(balance);
    }
    // async getDeltaBalance(): TokenBalance{
    //
    // }
    async getEffectiveGasPrice(transactionType: TransactionType): Promise<BN>{
        const medianPrice = await this.web3.eth.getGasPrice();
        const bnPriceMul = this.web3.utils.toBN(priceMul);
        const bnMedianPrice = this.web3.utils.toBN(medianPrice);
        const bnMuchLimit = this.web3.utils.toBN(muchLimit);
        const possiblePrice = bnMedianPrice.mul(bnPriceMul);
        if (possiblePrice.gte(bnMuchLimit))
            throw new Error('too much price');
        if (possiblePrice.lt(this.web3.utils.toBN('0')))
            throw new Error('need be positive');
        return possiblePrice;
    }
    // async arbitrageOrders(orderAsks, orderBids){
    //     orderAsks.sort((a, b) => {
    //         return a.amountGive .amountGive - b.amountGive
    //     });
    // }
    // async tradeAsk(ask){
    //
    // }
    // async tradeBid(bid){
    //
    // }
}

module.exports = Trader;