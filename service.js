const io = require('socket.io-client');
const Web3 = require('web3');
const BigNumber = require('bignumber.js');
// const BigNumber = Web3.utils.BN;
const sha256 = require('./utils/formatData').sha256;
const tokenAddr = {addr: '0x419c4db4b9e25d6db2ad9691ccb832c8d9fda05e', decimals: 18};
let possibleInteresting = [];
const lodash = require('lodash');
const fs = require('fs');
const Telegraf = require('telegraf');
var HttpsProxyAgent = require('https-proxy-agent');
var url = require('url');

const chatIds = [124442878];


const BOT_KEY = '413996458:AAFKgXYCj-UpuQPmnNL4e9MwJPXuXBOecMs';

const bot = new Telegraf(BOT_KEY)
bot.start((ctx) => {
    console.log('started:', ctx.from.id)
    return ctx.reply('Welcome!')
})
bot.startPolling()


const orderMap = {

};


const servers = [
    'https://socket01.etherdelta.com',
    'https://socket02.etherdelta.com',
    'https://socket03.etherdelta.com',
    'https://socket04.etherdelta.com',
    'https://socket05.etherdelta.com',
    'https://socket06.etherdelta.com'
];

const sleep = (time) => new Promise((r) => setTimeout(r, time));

const socketPool = {};
const getClientByToken = (token) => {
    const socketUrl = servers[Math.floor(Math.random()*servers.length)];
    if (!socketPool[token])
    {
        var username = 'lum-customer-hl_587681f9-zone-zone1';
        var password = 'm1arf8cgewt2';
        var session_id = (1000000 * Math.random())|0;
        var port = 22225;
        var super_proxy = 'http://'+username+'-session-'+session_id+':'+password+'@zproxy.luminati.io:'+port;
        var agent = new HttpsProxyAgent(url.parse(super_proxy));
        // agent.proxy = super_proxy;
        const socket = io.connect(socketUrl, { transports: ['websocket'], agent });
        socket.on('connect', () => {
            console.log('socket connected', token, socketUrl);
        });
        socket.on('disconnect', () => {
            console.log('socket disconnected', token, socketUrl);
        });
        socketPool[token] = socket;
    }
    return socketPool[token];
};

function Service() {
    const self = this;

    self.init = config => new Promise((resolve, reject) => {
        self.config = config;
        self.state = {
            orders: {},
            trades: undefined,
            myOrders: undefined,
            myTrades: undefined,
            listeners: {}
        };
        self.lastUpdate = {};

        // self.socket = io.connect(self.config.socketURL, { transports: ['websocket'] });
        // self.socket.on('connect', () => {
        //     console.log('socket connected');
        //     resolve();
        // });
        //
        // self.socket.on('disconnect', () => {
        //     console.log('socket disconnected');
        // });
        //
        // setTimeout(() => {
        //     reject('Could not connect to socket');
        // }, 10000);
    });

    self.getMarket = (token) => {
        if (!token || !Web3.utils.isAddress(token.addr)) throw new Error('Please enter a valid token');
        const socket = getClientByToken(token.addr);
        socket.emit('getMarket', { token: token.addr });
        console.log('emit get market');
    };

    self.waitForMarket = (token, isActive) => new Promise((resolve, reject) => {
        if (self.state.listeners[token.addr])
            return;
        self.state.listeners[token.addr] = true;
        self.lastUpdate[token.addr] = 0;
        const socket = getClientByToken(token.addr);
        setTimeout(() => {
            reject('Could not get market');
        }, 20000);
        // self.state = {
        //     orders: undefined,
        //     trades: undefined,
        //     myOrders: undefined,
        //     myTrades: undefined,
        // };
        socket.off('orders');
        // self.socket.off('trades');
        const getMarketAndWait = () => {
            self.getMarket(token);
            socket.once('market', async (market) => {

                if ((market.orders)) {
                    console.log('tickers', market.returnTicker);
                    if (market.returnTicker)
                    {
                        possibleInteresting = [];
                        Object.keys(market.returnTicker).forEach((k) => {
                            const ticker = market.returnTicker[k];
                            if (ticker.ask < ticker.bid)
                                possibleInteresting.push({...ticker, ticker: k});
                        });
                        console.log(possibleInteresting);
                        for (let x of possibleInteresting)
                        {
                            self.waitForMarket({addr: x.tokenAddr}).catch(console.error);
                            await sleep(2000);
                        }
                    }
                    else
                    {
                        setTimeout(() => {
                            if (possibleInteresting.length == 0)
                            {
                                console.log('cannot get market tickers');
                                process.exit(1);
                            }
                            getMarketAndWait();
                        }, 12000);
                    }
                    self.updateOrders(market.orders, token);
                    // self.updateTrades(market.trades, token, user);
                    socket.on('orders', (orders) => {
                        self.updateOrders(orders, token);
                    });
                    resolve();
                } else {
                    console.log('market answer', market);
                    setTimeout(() => {
                        if (possibleInteresting.length == 0)
                        {
                            console.log('cannot get market tickers');
                            process.exit(1);
                        }
                        getMarketAndWait();
                    }, 5000);
                }
            });
        };
        getMarketAndWait();
    });

    self.updateOrders = (newOrders, token) => {
        self.lastUpdate[token.addr] = Date.now();
        const minOrderSize = 0.001;
        console.log(newOrders.buys[0])
        console.log(newOrders.sells[0])
        const newOrdersTransformed = {
            buys: newOrders.buys
                .map(x =>
                    Object.assign({}, {
                        id: x.id,
                        date: new Date(x.updated),
                        price: new BigNumber(x.price),
                        amountGet: new BigNumber(x.amountGet),
                        amountGive: new BigNumber(x.amountGive),
                        deleted: x.deleted,
                        expires: Number(x.expires),
                        nonce: Number(x.nonce),
                        tokenGet: x.tokenGet,
                        tokenGive: x.tokenGive,
                        user: x.user,
                        r: x.r,
                        s: x.s,
                        v: x.v ? Number(x.v) : undefined,
                        // amount: self.toEth(x.amountGet, token.decimals).toNumber(),
                        // amountBase: self.toEth(x.amountGive, 18).toNumber(),
                        availableVolume: Number(x.availableVolume),
                        ethAvailableVolume: Number(x.ethAvailableVolume),
                        availableVolumeBase: Number(x.availableVolumeBase),
                        ethAvailableVolumeBase: Number(x.ethAvailableVolumeBase),
                    })),
            sells: newOrders.sells
                .map(x =>
                    Object.assign({}, {
                        id: x.id,
                        date: new Date(x.updated),
                        price: new BigNumber(x.price),
                        amountGet: new BigNumber(x.amountGet),
                        amountGive: new BigNumber(x.amountGive),
                        deleted: x.deleted,
                        expires: Number(x.expires),
                        nonce: Number(x.nonce),
                        tokenGet: x.tokenGet,
                        tokenGive: x.tokenGive,
                        user: x.user,
                        r: x.r,
                        s: x.s,
                        v: x.v ? Number(x.v) : undefined,
                        // amount: self.toEth(x.amountGive, token.decimals).toNumber(),
                        // amountBase: self.toEth(x.amountGet, 18).toNumber(),
                        availableVolume: Number(x.availableVolume),
                        ethAvailableVolume: Number(x.ethAvailableVolume),
                        availableVolumeBase: Number(x.availableVolumeBase),
                        ethAvailableVolumeBase: Number(x.ethAvailableVolumeBase),
                    })),
        };

        newOrdersTransformed.buys.forEach((x) => {
            if (!self.state.orders[x.tokenGet]) self.state.orders[x.tokenGet] = { buys: [], sells: [] };
            if (x.deleted || x.ethAvailableVolumeBase <= minOrderSize) {
                self.state.orders[x.tokenGet].buys = self.state.orders[x.tokenGet].buys.filter(y => y.id !== x.id);
                // if (x.user.toLowerCase() === user.addr.toLowerCase()) {
                //     self.state.myOrders.buys = self.state.myOrders.buys.filter(y => y.id !== x.id);
                // }
            } else if (self.state.orders[x.tokenGet].buys.find(y => y.id === x.id)) {
                self.state.orders[x.tokenGet].buys = self.state.orders[x.tokenGet].buys.map(y => (y.id === x.id ? x : y));
                // if (x.user.toLowerCase() === user.addr.toLowerCase()) {
                //     self.state.myOrders.buys = self.state.myOrders.buys.map(y => (y.id === x.id ? x : y));
                // }
            } else {
                self.state.orders[x.tokenGet].buys.push(x);
                // if (x.user.toLowerCase() === user.addr.toLowerCase()) {
                //     self.state.myOrders.buys.push(x);
                // }
            }
        });
        newOrdersTransformed.sells.forEach((x) => {
            if (!self.state.orders[x.tokenGive]) self.state.orders[x.tokenGive] = { buys: [], sells: [] };
            if (x.deleted || x.ethAvailableVolumeBase <= minOrderSize) {
                self.state.orders[x.tokenGive].sells = self.state.orders[x.tokenGive].sells.filter(y => y.id !== x.id);
                // if (x.user.toLowerCase() === user.addr.toLowerCase()) {
                //     self.state.myOrders.sells = self.state.myOrders.sells.filter(y => y.id !== x.id);
                // }
            } else if (self.state.orders[x.tokenGive].sells.find(y => y.id === x.id)) {
                self.state.orders[x.tokenGive].sells = self.state.orders[x.tokenGive].sells.map(y => (y.id === x.id ? x : y));
                // if (x.user.toLowerCase() === user.addr.toLowerCase()) {
                //     self.state.myOrders.sells = self.state.myOrders.sells.map(y => (y.id === x.id ? x : y));
                // }
            } else {
                self.state.orders[x.tokenGive].sells.push(x);
                // if (x.user.toLowerCase() === user.addr.toLowerCase()) {
                //     self.state.myOrders.sells.push(x);
                // }
            }
        });
        // self.state.orders = {
        //     sells: self.state.orders.sells.sort((a, b) =>
        //         a.price - b.price || a.amountGet - b.amountGet),
        //     buys: self.state.orders.buys.sort((a, b) =>
        //         b.price - a.price || b.amountGet - a.amountGet),
        // };
        // self.state.myOrders = {
        //     sells: self.state.myOrders.sells.sort((a, b) =>
        //         a.price - b.price || a.amountGet - b.amountGet),
        //     buys: self.state.myOrders.buys.sort((a, b) =>
        //         b.price - a.price || b.amountGet - a.amountGet),
        // };

        // const bid = self.state.orders.sells.length > 0 ? self.state.orders.sells[0] : 0;
        // const ask = self.state.orders.buys.length > 0 ? self.state.orders.buys[0] : 99999999999999;
        let b = self.state.orders[token.addr].buys.slice();
        // b = b.sort((a, b) => (a.price.gt(b.price) ? -1 : (a.price.eq(b.price) ? 0 : 1)) )
        //     .slice(0, 20);

        let s = self.state.orders[token.addr].sells.slice();
        // s = s.sort((a, b) => (a.price.gt(b.price) ? 1 : (a.price.eq(b.price) ? 0 : -1)) )
        //     .slice(0, 20);

        const limitToGoodTrade = new BigNumber('0.002');

        let goodLookingTrades = [];

        console.time('trades');
        for (let addr in self.state.orders)
        {
            const b = self.state.orders[addr].buys;
            const s = self.state.orders[addr].sells;
            for (let ask of b)
            {
                for (let bid of s)
                {
                    const price = ask.price.minus(bid.price);
                    if (price.lte(0))
                        continue;
                    const volume = new BigNumber(Math.min(ask.ethAvailableVolumeBase, bid.ethAvailableVolumeBase).toString());
                    const goodTrade = price.mul(volume);
                    goodLookingTrades.push({bid, ask, goodTrade, volume: volume, token: ask.tokenGet});
                }
            }
        }



        console.timeEnd('trades');
        console.log('trades', goodLookingTrades.length, goodLookingTrades.slice(0, 3).map(x => x.goodTrade.toString()));
        console.log(
            'current cryptos',
            Object.keys(self.state.orders)
                .map(x => ({token: x, lastUpdate: self.lastUpdate[x]}))
        );
        // console.log('all trades', goodLookingTrades.map(x => ({trade: x.goodTrade.toString(), ask: x.ask, bid: x.bid}) ));
        goodLookingTrades = goodLookingTrades
            .filter(x => limitToGoodTrade.lte(x.goodTrade))
            .sort((a, b) => {
                return (a.goodTrade.gt(b.goodTrade) ? -1 : (a.goodTrade.eq(b.goodTrade) ? 0 : 1));
            });
        if (goodLookingTrades.length)
        {
            for (let tr of goodLookingTrades)
            {
                const key = tr.ask.id + '_'+tr.bid.id;
                if (!orderMap[key])
                {
                    const m = JSON.stringify({trade: tr.goodTrade.toString(), volume: tr.volume.toString(), ask: tr.ask, bid: tr.bid, time: new Date()})+'\n';
                    fs.appendFileSync(
                        'good.log',
                        m,
                        {encoding: 'utf-8'}
                    );
                    chatIds.forEach((id) => {
                        bot.telegram.sendMessage(id, m);
                    });
                    orderMap[key] = true;
                }
            }


        }
        console.log('good looking trades', goodLookingTrades.map(x => ({trade: x.goodTrade.toString(), volume: x.volume.toString(), ask: x.ask, bid: x.bid}) ));


        //
        // console.log(
        //     'orders', token,
        //     'buys', b.map(x => ({price: x.price.toString(10), volume: x.ethAvailableVolumeBase.toString(10)})),
        //     'sells', s.map(x => ({price: x.price.toString(10), volume: x.ethAvailableVolumeBase.toString(10)}))
        // );
    };


    self.printOrderBook = () => {
        console.log('Order book');
        const ordersPerSide = 10;
        const sells = self.state.orders.sells.slice(0, ordersPerSide).reverse();
        const buys = self.state.orders.buys.slice(0, ordersPerSide);
        sells.forEach((order) => {
            console.log(`${order.price.toFixed(9)} ${order.ethAvailableVolume.toFixed(3)}`);
        });
        if (buys.length > 0 && sells.length > 0) {
            console.log(`---- Spread (${(sells[sells.length - 1].price - buys[0].price).toFixed(9)}) ----`);
        } else {
            console.log('--------');
        }
        buys.forEach((order) => {
            console.log(`${order.price.toFixed(9)} ${order.ethAvailableVolume.toFixed(3)}`);
        });
    };

    self.toEth = (wei, decimals) => new BigNumber(String(wei))
        .div(new BigNumber(10 ** decimals));
    self.toWei = (eth, decimals) => new BigNumber(String(eth))
        .times(new BigNumber(10 ** decimals)).floor();
}

module.exports = Service;


var s= new Service();
s.init({socketURL: 'https://socket05.etherdelta.com'});
s.waitForMarket(tokenAddr, true).catch(console.error);