require('babel-core/register');
require("babel-core").transform("code", {
    plugins: ["transform-flow-strip-types"]
});
require('./app.js');