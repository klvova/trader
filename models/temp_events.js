'use strict';
module.exports = function (sequelize, DataTypes) {
    var TempEvents = sequelize.define('TempEvents', {
        type: DataTypes.STRING,
        event: DataTypes.JSONB
    }, {
         classMethods: {
             associate: function (models) {
                 // associations can be defined here
             }
         }
    });
    return TempEvents;
};
