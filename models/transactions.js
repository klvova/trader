'use strict';
module.exports = function (sequelize, DataTypes) {
    var Transaction = sequelize.define('Transactions', {
        transaction_hash: DataTypes.STRING,
        removed: DataTypes.BOOELAN
    }, {
      classMethods: {
          associate: function (models) {
              // associations can be defined here
          }
      }
    });
    return Transaction;
};
