'use strict';
module.exports = function (sequelize, DataTypes) {
    var Order = sequelize.define('Order', {
        tokenGet: DataTypes.STRING,
        amountGet: DataTypes.DECIMAL(120, 20),
        tokenGive: DataTypes.STRING,
        amountGive: DataTypes.DECIMAL(120, 20),
        expires: DataTypes.DECIMAL(120, 20),
        nonce: DataTypes.STRING,
        deleted: DataTypes.BOOLEAN,
        user: DataTypes.STRING,
        hash: DataTypes.STRING,
        timestamp: DataTypes.INTEGER,
        actualAmountGive: DataTypes.DECIMAL(120, 20),
        actualAmountGet: DataTypes.DECIMAL(120, 20),
        transaction_hash: DataTypes.STRING
    }, {
       classMethods: {
           associate: function (models) {
               // associations can be defined here
           }
       }
    });
    return Order;
};
