// @flow
const assert = require('assert');

class Streamer{
    text: string;
    constructor(text: string){
        this.text = text;
    }
    fetch(charCount: number){
        assert.ok(typeof charCount === 'number', 'should be number');
        assert.ok(charCount > 0, 'should be greater than null');
        assert.ok(charCount < this.text.length, 'should be less than rest text length');

        const text = this.text.slice(0, charCount);
        this.text = this.text.slice(charCount);
        return text;
    }
}

module.exports = Streamer;