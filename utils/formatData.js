const Web3 = require('web3');
const crypto = require('crypto');

function getOrderDataStructure(order) {
    return {
        tokenGet: order.tokenGet,
        amountGet: order.amountGet,
        tokenGive: order.tokenGive,
        amountGive: order.amountGive,
        expires: order.expires,
        nonce: order.nonce,
        deleted: order.deleted,
        user: order.user,
        hash: order.hash,
        transaction_hash: order.transaction_hash || ''
    }
}

function transformUint256ToDec(hex) {
    const dec = Web3.utils.toBN(hex).toString();
    return dec;
}

function sha256(...params) {
    let key = params.join('');
    const hash = crypto.createHash('sha256');
    hash.update(key);
    return hash.digest('hex');
}

module.exports = {
    getOrderDataStructure,
    transformUint256ToDec,
    sha256
}