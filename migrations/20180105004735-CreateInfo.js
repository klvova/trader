'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    return queryInterface.createTable('Orders', {
        id: {
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
            type: Sequelize.INTEGER
        },
        author: {
            type: Sequelize.STRING,
            allowNull: false
        },
        tokenGet: {
            type: Sequelize.STRING,
            allowNull: false
        },
        amountGet: {
            type: Sequelize.DECIMAL(40, 20),
            allowNull: false
        },
        tokenGive: { type: Sequelize.STRING, allowNull: false },
        amountGive: { type: Sequelize.DECIMAL(40, 20), allowNull: false },
        expires: {
            type: Sequelize.DECIMAL(40, 20),
            allowNull: false
        },
        nonce: {
            type: Sequelize.STRING,
            allowNull: false
        },
        deleted: {
            type: Sequelize.BOOLEAN,
            allowNull: false
        }
    });
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    return queryInterface.dropTable('Orders')
  }
};
