'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      await queryInterface.addColumn('Orders', 'actualAmountGet', {
          type: Sequelize.DECIMAL(40, 20),
          allowNull: false
      });

      await queryInterface.addColumn('Orders', 'actualAmountGive', {
          type: Sequelize.DECIMAL(40, 20),
          allowNull: false
      });
  },

  down: async (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
      await queryInterface.removeColumn('Orders', 'actualAmountGet');
      await queryInterface.removeColumn('Orders', 'actualAmountGive');
  }
};
