'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    await queryInterface.changeColumn('Orders', 'amountGive', {type: Sequelize.DECIMAL(120, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'amountGet', {type: Sequelize.DECIMAL(120, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGive', {type: Sequelize.DECIMAL(120, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGet', {type: Sequelize.DECIMAL(120, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'expires', {type: Sequelize.DECIMAL(120, 20), allowNull: false});
  },

  down: async (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    await queryInterface.changeColumn('Orders', 'amountGive', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'amountGet', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGive', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGet', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'expires', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
  }
};
