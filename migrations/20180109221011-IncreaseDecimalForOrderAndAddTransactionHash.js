'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
    await queryInterface.addColumn('Orders', 'transaction_hash', {type: Sequelize.STRING, allowNull: false});
    await queryInterface.changeColumn('Orders', 'amountGive', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'amountGet', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGive', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGet', {type: Sequelize.DECIMAL(60, 20), allowNull: false});
    await queryInterface.addIndex('Orders', {fields: ['transaction_hash'], name: 'transaction_hash_index', type: 'UNIQUE'});
  },

  down: async (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
    await queryInterface.removeColumn('Orders', 'transaction_hash');
    await queryInterface.changeColumn('Orders', 'amountGive', {type: Sequelize.DECIMAL(40, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'amountGet', {type: Sequelize.DECIMAL(40, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGive', {type: Sequelize.DECIMAL(40, 20), allowNull: false});
    await queryInterface.changeColumn('Orders', 'actualAmountGet', {type: Sequelize.DECIMAL(40, 20), allowNull: false});
    await queryInterface.removeIndex('Orders', 'transaction_hash_index');
  }
};
