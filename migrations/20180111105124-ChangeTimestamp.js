'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
	await queryInterface.removeColumn('Orders', 'timestamp');
	await queryInterface.addColumn('Orders', 'timestamp', {
		type: Sequelize.DATE,
		allowNull: false,
		defaultValue: null
	})
  },

  down: (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
	return queryInterface.changeColumn('Orders', 'timestamp', {
		type: Sequelize.INTEGER,
		allowNull: false,
		defaultValue: null
	})
  }
};
