'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      await queryInterface.addColumn('Orders', 'createdAt', {
            allowNull: false,
            type: Sequelize.DATE
      });
      await queryInterface.addColumn('Orders', 'updatedAt', {
          allowNull: false,
          type: Sequelize.DATE
      });
  },

  down: async (queryInterface, Sequelize) => {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.dropTable('users');
    */
     await queryInterface.removeColumn('Orders', 'createdAt');
     await queryInterface.removeColumn('Orders', 'updatedAt');
  }
};
