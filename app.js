//@flow
const net = require('net');
const { launchDeleteInvalidOrders } = require('./deleteInvalidOrders');
const Web3 = require('web3');
const abiArray = require('./abi.js');
const contractAddress = '0x8d12A197cB00D4747a1fe03395095ce2A5CC6819';
import EventsListener  from './eventsListener';
const web3 = new Web3('/root/jsonrpc.ipc', net);

web3.eth.isSyncing().then(console.log);

const contract = new web3.eth.Contract(abiArray, contractAddress);

const listener = new EventsListener(contract, web3);

const launchEvents = () => {
    listener.cancel();
    listener.trade();
    listener.order();
}

launchEvents();

launchDeleteInvalidOrders();

// contract.events.Trade({
// 	fromBlock: 4*(10^6)
// }).on('data', console.log);

// web3.eth.getTransaction('0x8169a23535799174a833680a55f92ff15a34e4b67192eb1af705a1d548470db7')
// 	.then(console.log);


