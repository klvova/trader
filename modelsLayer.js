const models = require('./models');
const { deletedTime } = require('./constants');
const { Order, Transaction } = models;
const sequelize = models.sequelize;
const Op = sequelize.Op;
const transaction = sequelize.transaction.bind(models.sequelize);

const createOrder = (order) => Order.create({
    ...order,
    actualAmountGet: order.amountGet,
    actualAmountGive: order.amountGive
})

const markOrderAsDeleted = (order) => Order.update({
            'deleted': true,
            'timestamp': new Date()
        }, {
            where: {
                'hash': order.hash,
                'user': order.user
            }
        })
        .catch((e) => {
            console.error(e);
        });

const deleteOrders = () => Order.destroy({
        where: {
            'deleted': true,
            'timestamp': {
                [Op.gte]: sequelize.literal(`NOW() - INTERVAL '10 minutes'`)
            }
        }
    })

const trade = async (tradeData) => {
    const tradeOrder = await Order.update({
        actualAmountGet: sequelize.literal(`"actualAmountGet" ${tradeData.removed ? '+' : '-'} ${tradeData.amountGet}`),
        actualAmountGive: sequelize.literal(`"actualAmountGive" ${tradeData.removed ? '+' : '-'} ${tradeData.amountGive}`)
    }, {
        where: {
            hash: tradeData.hash,
            user: tradeData.user
        }
    });
};

const createTransaction = (transaction_hash) => {
    return Transaction.create({
        transaction_hash
    });
};

const removeTransaction = (transaction_hash) => {
    return Transaction.destroy({
        where: {
            transaction_hash
        }
    });
}

module.exports = {
    createOrder,
    markOrderAsDeleted,
    deleteOrders,
    trade,
    createTransaction,
    removeTransaction
}
