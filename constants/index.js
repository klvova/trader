const APP_SETTINGS = {
    fromBlock: 4900000,
    deletedTime: 10*60*1000
};

module.exports = APP_SETTINGS;
