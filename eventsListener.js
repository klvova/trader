//@flow
const {
    getOrderDataStructure,
    transformUint256ToDec,
    sha256
} = require('./utils/formatData');

const {
    createOrder,
    markOrderAsDeleted,
    trade,
    createTransaction,
    removeTransaction
} = require('./modelsLayer');

import { createRow } from './syncWithTempData';

const Streamer = require('./utils/streamer');

const APP_SETTINGS = require('./constants');

export const _createOrder = (event: Object) => {
    const returnValues = event.returnValues;
    const order = {
        ...event.returnValues,
        hash: sha256(
            returnValues.tokenGet,
            returnValues.amountGet,
            returnValues.tokenGive,
            returnValues.amountGive,
            returnValues.expires,
            returnValues.nonce
        ),
        transaction_hash: event.transactionHash
    };
    return createOrder(getOrderDataStructure(order));
};

export const _cancelOrder = (event: Object) => {
    const order = event.returnValues;
    const hash = sha256(
        order.tokenGet,
        order.amountGet,
        order.tokenGive,
        order.amountGive,
        order.expires,
        order.nonce
    );

    return markOrderAsDeleted(getOrderDataStructure({
        ...order,
        hash
    }));
};

class Listener{
    contract: Contract;
    web3: Web3Interface;
    constructor(contract: Contract, web3: Web3Interface){
        this.contract = contract;
        this.web3 = web3;
    }
    order(orderCb?: Object){
        this.contract.events.Order({
            fromBlock: APP_SETTINGS.fromBlock
        })
            .on('data', (event) => {
                createRow('create_order', event);
                _createOrder(event);
            })
            .on('changed', (event) => {
                createRow('cancel_order', event);
                _cancelOrder(event);
            })
            .on('error', console.error);
    }
    cancel(){
        this.contract.events.Cancel({
            fromBlock: APP_SETTINGS.fromBlock
        })
            .on('data', (event) => {
                createRow('cancel_order', event);
                _cancelOrder(event);
            })
            .on('error', console.error);
    }
    async tradeHandler(event: ContractEvent){
        console.log('======trade = ', event);
        console.log('event.transactionHash = ', event.transactionHash);
        const transactionData = await this.web3.eth.getTransaction(event.transactionHash);
        console.log('transactionData for trade ===', transactionData);
        const startPoint = 10;
        const inputArgumentSize = 16;
        const inputData = new Streamer(transactionData.input);
        const methodId = inputData.fetch(startPoint);
        const tokenGet = inputData.fetch(inputArgumentSize);
        const amountGet = transformUint256ToDec(inputData.fetch(inputArgumentSize));
        const tokenGive = inputData.fetch(inputArgumentSize);
        const amountGive = transformUint256ToDec(inputData.fetch(inputArgumentSize));
        const expires = transformUint256ToDec(inputData.fetch(inputArgumentSize));
        const nonce = inputData.fetch(inputArgumentSize);
        const user = inputData.fetch(inputArgumentSize);
        const v = inputData.fetch(inputArgumentSize);
        const r = inputData.fetch(inputArgumentSize);
        const s = inputData.fetch(inputArgumentSize);
        const amount = transformUint256ToDec(inputData.fetch(inputArgumentSize));
        trade({
            hash: sha256(tokenGet, amountGet, tokenGive, amountGive, expires, nonce),
            author: user,
            amountGet: event.returnValues.amountGet,
            amountGive: event.returnValues.amountGive,
            removed: event.removed
        });
    }
    trade(){
        this.contract.events.Trade({
            fromBlock: APP_SETTINGS.fromBlock
        })
            .on('data', (event) => {
                createRow('create_trade', event);
                this.tradeHandler(event);
                createTransaction(event.transactionHash);
            })
            .on('changed', (event) => {
                createRow('cancel_trade', event);
                this.tradeHandler(event);
                removeTransaction(event.transactionHash);
            })
            .on('error', console.error);
    }
}

export default Listener;
